# Python 3.x
# Javier Rivera
# Recorriendo y mostrando los elementos de un Arreglo tipo lista

a = [6,4,2,2,4,5,7,3,2]

# Recorrido con un for
for valor in a:
	print (valor)
	
# Recorrido con un while
pos = 0
while (pos < len(a)):
	print (a[pos])
	pos = pos + 1